
<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Document</title>
</head>
<body>

<h3>Full Address</h3>
<p><?php echo getFullAddress("Philippines", "Makati City", "Metro Manila", "3F Enzo Bldg., Buendia Avenue") ?></p>
<p><?php echo getFullAddress("Philippines", "Quezon City", "Metro Manila", "3F Caswyn Bldg., Timog Avenue") ?></p>
	
<h3>Letter-Based Grading</h3>
<p><?php echo getLetterGrade(98) ?></p>
<p><?php echo getLetterGrade(95) ?></p>
<p><?php echo getLetterGrade(92) ?></p>
<p><?php echo getLetterGrade(89) ?></p>
<p><?php echo getLetterGrade(86) ?></p>
<p><?php echo getLetterGrade(83) ?></p>
<p><?php echo getLetterGrade(80) ?></p>
<p><?php echo getLetterGrade(77) ?></p>
<p><?php echo getLetterGrade(75) ?></p>
<p><?php echo getLetterGrade(74) ?></p>
</body>
</html>